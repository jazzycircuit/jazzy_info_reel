from functools import partial
from logging import getLogger
from os.path import dirname, realpath

from jazzy_info_reel_server import (
        InfoReelHTTPServer, InfoReelHTTPRequestHandler, configure_logger,
        read_config)


CONF_DEFAULTS_RELPATH = "config/default.conf"
CONF_RELPATH = "config/settings.conf"
LOGDIR_RELPATH = "logs"
LOGGER_NAME = "jazzy-info-reel"
STARTGG_ACCESS_TOKEN_RELPATH = "config/startgg-access-token.txt"
GIVEBUTTER_API_KEY_RELPATH = "config/givebutter-api-key.txt"
WEB_DIRECTORY_RELPATH = "web"


def main():
    logger = getLogger(LOGGER_NAME)
    server_dir = dirname(realpath(__file__))
    logfile_dir = f"{server_dir}/{LOGDIR_RELPATH}"
    conf_defaults_path = f"{server_dir}/{CONF_DEFAULTS_RELPATH}"
    conf_path = f"{server_dir}/{CONF_RELPATH}"
    config = read_config(conf_defaults_path, conf_path, logger)
    configure_logger(logger, logfile_dir, config["debug_logging"])
    startgg_token_path = f"{server_dir}/{STARTGG_ACCESS_TOKEN_RELPATH}"
    logger.info(f"reading start.gg access token from {startgg_token_path}")
    with open(startgg_token_path) as f:
        startgg_access_token = f.read().strip()
    givebutter_key_path = f"{server_dir}/{GIVEBUTTER_API_KEY_RELPATH}"
    logger.info(f"reading givebutter api key from {givebutter_key_path}")
    with open(givebutter_key_path) as f:
        givebutter_api_key = f.read().strip()
    web_dir = f"{server_dir}/{WEB_DIRECTORY_RELPATH}"
    DirectoryHandler = partial(
            InfoReelHTTPRequestHandler, directory=web_dir)
    givebutter_cache_seconds = int(config["givebutter_cache_seconds"])
    server = InfoReelHTTPServer(
            start_league=config["start_league"],
            startgg_access_token=startgg_access_token,
            events_cache_seconds=int(config["events_cache_seconds"]),
            givebutter_api_key=givebutter_api_key,
            givebutter_cache_seconds=givebutter_cache_seconds,
            web_dir=web_dir, server_address=("", int(config["port"])),
            RequestHandlerClass=DirectoryHandler)
    logger.info("serving forever")
    with server:
        server.serve_forever()
        
        
if __name__ == "__main__":
    main()

const PREVIOUS_SLIDE_DELAY = 600;

class Slide {
    constructor({slideType, slidePath, durationMs, videoVolume = 1}) {
        this.initializeNode(slideType, slidePath, videoVolume);
        switch(this.node.tagName) {
            case "VIDEO":
                this.loadEvent = "canplaythrough";
                break;
            default: this.loadEvent = "load";
        }
        this.loaded = false;
        this.node.addEventListener(
            this.loadEvent, function (){this.loaded = true;}.bind(this)
        );
        this.durationMs = durationMs;
    }
    initializeNode(slideType, slidePath, videoVolume) {
        switch (slideType) {
            case "image":
                this.node = document.createElement("img");
                break;
            case "iframe":
                this.node = document.createElement("iframe");
                this.node.loading = "eager";
                break;
            case "video":
                this.node = document.createElement("video");
                this.node.volume = videoVolume;
        }
        this.node.src = slidePath;
        this.node.classList.add("slide");
        this.node.classList.add("behind-curtain");
    }
    activate() {
        this.node.classList.add("active-slide");
        this.node.classList.remove("behind-curtain");
        if(this.node.tagName == "VIDEO") {this.node.play();}
    }
    becomePrevious() {
        this.node.classList.add("previous-slide");
        this.node.classList.remove("active-slide");
    }
    reset() {
        this.node.classList.add("behind-curtain");
        this.node.classList.remove("previous-slide");
        switch(this.node.tagName) {
            case "IFRAME":
                this.loaded = false;
                this.node.contentWindow.location.reload();
                break;
            case "VIDEO":
                this.node.pause();
                this.node.currentTime = 0;
        }
    }
    deactivate() {
        this.becomePrevious();
        setTimeout(this.reset.bind(this), PREVIOUS_SLIDE_DELAY);
    }
}
class SlideShow {
    constructor(slidesData) {
        this.slides = [];
        this.slideIndex = -1;
        this.node = document.createElement("div");
        this.node.classList.add("slideshow");
        let curtain = document.createElement("div");
        curtain.classList.add("curtain");
        this.node.appendChild(curtain);
        slidesData.forEach(this.initializeSlide.bind(this));
    }
    initializeSlide(slideData) {
        let slide = new Slide(slideData);
        this.slides.push(slide);
        this.node.appendChild(slide.node);
    }
    loop() {
        let nextIndex = this.slideIndex + 1;
        if(nextIndex >= this.slides.length) {nextIndex = 0;}
        let nextSlide = this.slides[nextIndex];
        if(nextSlide.loaded == false) {
            let onLoad = function () {
                nextSlide.loaded = true;
                this.loop();
            }
            nextSlide.node.addEventListener(
                nextSlide.loadEvent, onLoad.bind(this), {"once": true}
            );
            return
        }
        if(this.slideIndex >= 0) {
            this.slides[this.slideIndex].deactivate();
        }
        nextSlide.activate();
        this.slideIndex = nextIndex;
        setTimeout(this.loop.bind(this), nextSlide.durationMs);
    }
}

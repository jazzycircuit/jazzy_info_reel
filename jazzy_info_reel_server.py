from configparser import ConfigParser
from http.server import HTTPServer, SimpleHTTPRequestHandler
from json import dumps as json_dumps
from logging import DEBUG, FileHandler, Formatter, getLogger, StreamHandler
from time import gmtime, strftime, time

from streambrain.clients.startgg import get_league_events
from streambrain.clients.givebutter import get_transactions


PARENT_LOGGER_NAME = "jazzy-info-reel"


class InfoReelHTTPServer(HTTPServer):
    def __init__(
            self, start_league: str, startgg_access_token: str,
            events_cache_seconds: int, givebutter_api_key: str,
            givebutter_cache_seconds: int, web_dir: str, *args, **kwargs):
        self._logger = getLogger(PARENT_LOGGER_NAME).getChild(f"{self}")
        self._start_league = start_league
        self._startgg_access_token = startgg_access_token
        self._events_cache_seconds = events_cache_seconds
        self._events_cache = []
        self._events_fetched_at = 0
        self._givebutter_api_key = givebutter_api_key
        self._givebutter_cache_seconds = givebutter_cache_seconds
        self._givebutter_cache = []
        self._givebutter_fetched_at = 0
        self.web_dir = web_dir
        super().__init__(*args, **kwargs)

    def get_league_events(self):
        if time() - self._events_fetched_at > self._events_cache_seconds:
            self._logger.info("getting league events from start.gg")
            try:
                self._events_cache = get_league_events(
                        self._startgg_access_token, self._start_league)
            except Exception as e:
                self._logger.warning(
                        f"During get_league_events, caught {e}. Falling "
                        "back on cache.")
            else:
                self._logger.info("got league events from start.gg")
                self._events_fetched_at = time()
        else:
            self._logger.info("returning league events from local cache")
        return self._events_cache

    def get_givebutter_donations(self):
        seconds_since_fetch = time() - self._givebutter_fetched_at
        if seconds_since_fetch > self._givebutter_cache_seconds:
            self._logger.info("getting transactions from givebutter")
            try:
                self._givebutter_cache = get_transactions(
                        self._givebutter_api_key)
            except Exception as e:
                self._logger.warning(
                        f"During get_transactions, caught {e}. Falling "
                        "back on cache.")
            else:
                self._logger.info("got transactions from givebutter")
                self._givebutter_fetched_at = time()
        else:
            self._logger.info("returning transactions from local cache")
        return self._givebutter_cache


class InfoReelHTTPRequestHandler(SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        self._logger = getLogger(PARENT_LOGGER_NAME).getChild(f"{self}")
        super().__init__(*args, **kwargs)

    def do_GET(self):
        self._logger.info(f"get request received for {self.path}")
        if self.path == "/league-events":
            self.do_league_events()
        elif self.path == "/donations":
            self.do_donations()
        elif self.path == "/donors":
            self.do_donors()
        else:
            super().do_GET()
        self._logger.info(f"get request processed for {self.path}")

    def do_league_events(self):
        self._logger.info("requesting league events from local server")
        events = self.server.get_league_events()
        content = bytes(json_dumps(events), "UTF-8")
        self.send_response(200)
        self.send_header("Content-type", "json")
        self.send_header("Content-Length", len(content))
        self.add_caching_headers()
        self.end_headers()
        self.wfile.write(content)

    def do_donations(self):
        self._logger.info(
                "requesting givebutter donations from local server")
        donations = self.server.get_givebutter_donations()
        donations_sterilized = []
        for transaction in donations:
            gs = transaction.giving_space
            donation = {
                    "donor_name": gs.donor_name, "amount": gs.amount,
                    "currency": transaction.currency, "message": gs.message}
            donations_sterilized.append(donation)
        content = bytes(json_dumps(donations_sterilized), "UTF-8")
        self.send_response(200)
        self.send_header("Content-type", "json")
        self.send_header("Content-Length", len(content))
        self.add_caching_headers()
        self.end_headers()
        self.wfile.write(content)

    def do_donors(self):
        self._logger.info(
                "requesting givebutter donations from local server")
        donations = self.server.get_givebutter_donations()
        donors = []
        processed_email_addresses = []
        for transaction in donations:
            donor = transaction.giving_space.donor_name
            if (
                    transaction.email in processed_email_addresses
                    or donor in donors):
                continue
            processed_email_addresses.append(transaction.email)
            donors.append(transaction.giving_space.donor_name)
        content = bytes(json_dumps(donors), "UTF-8")
        self.send_response(200)
        self.send_header("Content-type", "json")
        self.send_header("Content-Length", len(content))
        self.add_caching_headers()
        self.end_headers()
        self.wfile.write(content)

    def add_caching_headers(self):
        now = strftime("%a, %d %b %Y %X", gmtime()) + " GMT"
        self.send_header("Expires", "Tue, 03 Jul 2001 06:00:00 GMT")
        self.send_header("Last-Modified", f"{now}")
        self.send_header(
                "Cache-Control",
                "max-age=0, no-cache, must-revalidate, proxy-revalidate, "
                "no-store")


def configure_logger(logger, logfile_dir: str, is_debug: bool):
    fmt = "{levelname}: {asctime}\n    {name}\n        {message}"
    formatter = Formatter(fmt=fmt, style="{")
    file_handler = FileHandler(
            f"{logfile_dir}/{strftime('%Y-%m-%d_%H-%M')}.log")
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    stream_handler = StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)
    if is_debug:
        logger.setLevel(DEBUG)
    logger.info(f"configured logfile @ {logfile_dir}/log.log")


def read_config(default_path: str, settings_path: str, logger):
    config_parser = ConfigParser()
    logger.info(f"reading config from {default_path}")
    config_parser.read(default_path)
    logger.info(f"read config from {default_path}")
    logger.info(f"reading config from {settings_path}")
    config_parser.read(settings_path)
    logger.info(f"read config from {settings_path}")
    return config_parser["settings"]
